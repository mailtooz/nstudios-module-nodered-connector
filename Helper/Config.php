<?php
namespace NStudios\NoderedConnector\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Config
 *
 * @package   NStudios_NoderedConnector
 * @author    Ozan Yavasoglulari <support@nstudios.uk>
 * @copyright 2018 NStudios
 */

class Config extends AbstractHelper
{
    const XML_PATH_CONFIG = 'nstudios_nodered/%s/%s';

    protected $scopeConfig;

    /**
     * Config constructor.
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    public function isOrderProcessingEnabled()
    {
        return $this->getConfig('order_processing_enable', 'general');
    }

    public function getAppId()
    {
        return $this->getConfig('app_id', 'general');
    }

    public function getBearerToken()
    {
        return $this->getConfig('bearer_token', 'general');
    }

    public function getServiceUrl()
    {
        return $this->getConfig('service_url', 'general');
    }

    public function getServicePort()
    {
        return $this->getConfig('service_port', 'general');
    }

    public function getXAutKey()
    {
        return $this->getConfig('xauth_key', 'general');
    }

    public function getXAuthUser()
    {
        return $this->getConfig('xauth_user', 'general');
    }

    public function getOrderSaveAfterWebhook()
    {
        return $this->getConfig('order_save_after_webhook', 'general');
    }

    public function isProductProcessingEnabled()
    {
        return $this->getConfig('product_processing_enable', 'general');
    }

    public function getProductGetWebhook()
    {
        return $this->getConfig('product_get_webhook', 'general');
    }
    
    protected function getConfig($field, $group)
    {
        return $this->scopeConfig->getValue(
            sprintf(self::XML_PATH_CONFIG, $group, $field),
            ScopeInterface::SCOPE_STORE
        );
    }
}
