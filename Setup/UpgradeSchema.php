<?php
namespace NStudios\NoderedConnector\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Upgrade Schema script class.
 *
 * @package NStudios_NoderedConnector
 * @author Ozan Yavasoglulari <support@nstudios.com>
 * @copyright 2018 NStudios (https://www.nstudios.com)
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var \Magento\Framework\Setup\SchemaSetupInterface
     */
    private $setup;

    /**
     * @var \Magento\Framework\Setup\ModuleContextInterface
     */
    private $context;

    /**
     * Upgrade script.
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @author Ozan Yavasoglulari <support@nstudios.com>
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->setup = $setup;
        $this->context = $context;
        $this->setup->startSetup();

        if (version_compare($this->context->getVersion(), '0.1.1', '<')) {
            $this->addNodeRedColumns();
            $this->addNodeRedColumnsToOrderGrid();
        }

        $this->setup->endSetup();
    }

    /**
     * Add Node-RED columns.
     *
     * @author Ozan Yavasoglulari <support@nstudios.com>
     */
    public function addNodeRedColumns()
    {
        $this->setup->getConnection()->addColumn(
            $this->setup->getTable('sales_order'),
            'node_red_processed',
            [
                'type'     => Table::TYPE_BOOLEAN,
                'nullable' => false,
                'default'  => 0,
                'comment'  => 'Node-RED processed',
            ]
        );

        $this->setup->getConnection()->addColumn(
            $this->setup->getTable('sales_order'),
            'node_red_sent',
            [
                'type'     => Table::TYPE_BOOLEAN,
                'nullable' => false,
                'default'  => 0,
                'comment'  => 'Node-RED sent',
            ]
        );

        $this->setup->getConnection()->addColumn(
            $this->setup->getTable('sales_order'),
            'node_red_error_message',
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => true,
                'default'  => null,
                'comment'  => 'Node-RED error message',
            ]
        );
    }

    /**
     * Add Node-RED columns to order grid
     *
     * @author Ozan Yavasoglulari <support@nstudios.com>
     */
    public function addNodeRedColumnsToOrderGrid()
    {
        $connection = $this->setup->getConnection();
        $salesOrderGridTable = $this->setup->getTable('sales_order_grid');
        $salesOrderTable = $this->setup->getTable('sales_order');

        $connection->addColumn(
            $salesOrderGridTable,
            'node_red_processed',
            [
                'type'     => Table::TYPE_BOOLEAN,
                'nullable' => false,
                'default'  => 0,
                'comment'  => 'Node-RED processed',
            ]
        );

        $connection->addColumn(
            $salesOrderGridTable,
            'node_red_sent',
            [
                'type'     => Table::TYPE_BOOLEAN,
                'nullable' => false,
                'default'  => 0,
                'comment'  => 'Node-RED sent',
            ]
        );

        $connection->addColumn(
            $salesOrderGridTable,
            'node_red_error_message',
            [
                'type'     => Table::TYPE_TEXT,
                'nullable' => true,
                'default'  => null,
                'comment'  => 'Node-RED error message',
            ]
        );

        //update newly added columns in order grid table
        $connection->query(
            $connection->updateFromSelect(
                $connection->select()
                    ->join(
                        $salesOrderTable,
                        sprintf(
                            '%s.entity_id = %s.entity_id',
                            $salesOrderGridTable,
                            $salesOrderTable
                        ),
                        ['node_red_processed', 'node_red_sent', 'node_red_error_message']
                    ),
                $salesOrderGridTable
            )
        );
    }
}
