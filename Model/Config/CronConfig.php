<?php
namespace NStudios\NoderedConnector\Model\Config;

use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\App\Config\ValueFactory;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

/**
 * Cron Config Class
 *
 * @package NStudios_NoderedConnector
 * @author Ozan Yavasoglulari <support@nstudios.com>
 * @copyright 2018 NStudios (https://www.nstudios.com)
 */

class CronConfig extends Value
{
    /**
     * Cron string path
     */
    const CRON_STRING_PATH = 'crontab/default/jobs/nstudios_noderedconnector_order_push/schedule/cron_expr';

    /**
     * Cron model path
     */
    const CRON_MODEL_PATH = 'crontab/default/jobs/nstudios_noderedconnector_order_push/run/model';

    /**
     * @var \Magento\Framework\App\Config\ValueFactory
     */
    private $configValueFactory;

    /**
     * @var string
     */
    private $runModelPath = '';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Config\ValueFactory $configValueFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param string $runModelPath
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $config,
        TypeListInterface $cacheTypeList,
        ValueFactory $configValueFactory,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        $runModelPath = '',
        array $data = []
    ) {
        $this->runModelPath = $runModelPath;
        $this->configValueFactory = $configValueFactory;
        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * {@inheritdoc}
     *
     * @return $this
     * @throws \Exception
     */
    public function afterSave()
    {
        try {
            if ($this->getData('groups/general/fields/order_processing_enable/value')) {
                $this->addCronJob();
            } else {
                $this->removeCronJob();
            }
        } catch (\Exception $e) {
            throw new LocalizedException(__('We can\'t save the cron expression.'));
        }

        return parent::afterSave();
    }

    /**
     * Add cron job configuration.
     *
     * @author Ozan Yavasoglulari <support@nstudios.com>
     */
    private function addCronJob()
    {
        $this->configValueFactory->create()
            ->load(self::CRON_STRING_PATH, 'path')
            ->setValue($this->getData('groups/general/fields/order_push_cron_expr/value'))
            ->setPath(self::CRON_STRING_PATH)
            ->save();

        $this->configValueFactory->create()
            ->load(self::CRON_MODEL_PATH, 'path')
            ->setValue($this->runModelPath)
            ->setPath(self::CRON_MODEL_PATH)
            ->save();
    }

    /**
     * Remove cron job configuration.
     *
     * @author Ozan Yavasoglulari <support@nstudios.com>
     */
    private function removeCronJob()
    {
        $this->configValueFactory->create()
            ->load(self::CRON_STRING_PATH, 'path')
            ->delete();

        $this->configValueFactory->create()
            ->load(self::CRON_MODEL_PATH, 'path')
            ->delete();
    }
}
