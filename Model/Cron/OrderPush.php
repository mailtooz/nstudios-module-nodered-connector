<?php
namespace NStudios\NoderedConnector\Model\Cron;

use NStudios\NoderedConnector\Helper\Config;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
//use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;

/**
 * Nodered Export Cron
 *
 * @package NStudios_NoderedConnector
 * @author Ozan Yavasoglulari <support@nstudios.com>
 * @copyright 2018 NStudios (https://www.nstudios.com)
 */
class OrderPush
{
    /**
     * @var \Nstudios\NoderedConnector\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $orderCollection;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $salesOrderRepo;

    /**
     * @param \Nstudios\NoderedConnector\Helper\Config $configHelper
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollection
     * @param \Magento\Sales\Api\OrderRepositoryInterface $salesOrderRepo
     */
    public function __construct(
        Config $configHelper,
        LoggerInterface $logger,
        CollectionFactory $orderCollection,
        OrderRepositoryInterface $salesOrderRepo
    ) {
        $this->configHelper = $configHelper;
        $this->logger = $logger;
        $this->orderCollection = $orderCollection;
        $this->salesOrderRepo = $salesOrderRepo;
    }

    /**
     * Get orders to push.
     *
     * @author Ozan Yavasoglulari <support@nstudios.com>
     */
    public function execute()
    {
        if (!$this->configHelper->isOrderProcessingEnabled()) {
            return false;
        }

        try {
            $collection = $this->orderCollection->create()
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('total_due')
                ->addAttributeToFilter('node_red_sent', '0')
                ->addAttributeToFilter('total_due', '0.0000');
            
            foreach ($collection as $orderItem) {
                $order = $this->salesOrderRepo->get($orderItem->getId());
                $curlResult = $this->pushOrderToNodeRed($orderItem->getId());
                
                $order->setNodeRedSent(true);
                if ($curlResult['error_message']) {
                    $order->setNodeRedProcessed(0);
                    $order->setNodeRedErrorMessage($curlResult['error_message']);
                } else {
                    $order->setNodeRedProcessed(1);
                    $order->setNodeRedErrorMessage('Success');
                }
                                
                try {
                    $this->salesOrderRepo->save($order);
                    $this->logger->log(600, print("ORDER ID: " . $orderItem->getId() . " SENT"));
                } catch (\Exception $e) {
                    $this->logger->log(600, print($e->getMessage()));
                }
                $curlResult = [];
                // $this->logger->log(600, print("NODERED SENT: " . $orderItem->getNodeRedSent()));
            }
        } catch (LocalizedException $e) {
            $this->logger->info("NSTUDIOS - " . $e->getMessage(), true);
        }
    }

    /**
     * Export order to nodered.
     *
     * @author Ozan Yavasoglulari <support@nstudios.com>
     * @param int $orderId
     * @return array
     */
    private function pushOrderToNodeRed(int $orderId)
    {
        $serviceUrl = $this->configHelper->getServiceUrl();
        $webhookPath = $this->configHelper->getOrderSaveAfterWebhook();

        $curl = curl_init();

        if ($this->configHelper->getServicePort()) {
            $curlOpts[CURLOPT_PORT] = $this->configHelper->getServicePort();
        }
        
        $curlOpts[CURLOPT_URL] =  $serviceUrl . $webhookPath;
        $curlOpts[CURLOPT_RETURNTRANSFER] =  true;
        $curlOpts[CURLOPT_ENCODING] =  "";
        $curlOpts[CURLOPT_MAXREDIRS] =  10;
        $curlOpts[CURLOPT_TIMEOUT] =  30;
        $curlOpts[CURLOPT_HTTP_VERSION] = CURL_HTTP_VERSION_1_1;
        $curlOpts[CURLOPT_CUSTOMREQUEST] =  "POST";
        $curlOpts[CURLOPT_POSTFIELDS] =  "order_id=" . $orderId;
        $curlOpts[CURLOPT_HTTPHEADER] =  array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "X-Auth-Key: " . $this->configHelper->getXAutKey(),
            "X-Auth-User: " . $this->configHelper->getXAuthUser(),
            "App-Id: " . $this->configHelper->getAppId(),
            "Token: " . $this->configHelper->getBearerToken()
        );

        curl_setopt_array($curl, $curlOpts);

        $response = curl_exec($curl);
        $errMsg = curl_error($curl);
        $errNo = curl_errno($curl);
        curl_close($curl);

        if ($errNo) {
            /**
             * If there is an error we need to requeue the order
             * so that it can be re-sent
             * $this->logger->debug("cURL Error #:" . $errMsg . ":" . $errNo);
             */
            $this->logger->info("cURL Response: " . $response . " Error: " . $errNo . ":" . $errMsg);
        }

        return ['response' => $response, 'error_message' => $errMsg];
    }
}
