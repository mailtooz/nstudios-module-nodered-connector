<?php
namespace NStudios\NoderedConnector\Observer\Sales\Order;

use NStudios\NoderedConnector\Helper\Config;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

/**
 * Sales Order Save After event to add sales order te RMQ job
 *
 * @package NStudios_NoderedConnector
 * @author Ozan Yavasoglulari <support@nstudios.com>
 * @copyright 2018 NStudios (https://www.nstudios.com)
 */
class AddToQueue implements ObserverInterface
{
    /**
     * @var \NStudios\NoderedConnector\Helper\Config
     */
    private $sysConfig;

    /**
     * @var $logger \Psr\Log\LoggerInterface;
     */
    private $logger;

    /**
     * @var $order \Magento\Sales\Model\Order
     */
    private $order;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    public function __construct(
        LoggerInterface $logger,
        Config $sysConfig,
        \Magento\Sales\Model\OrderFactory $orderFactory
    ) {
        $this->logger = $logger;
        $this->sysConfig = $sysConfig;
        $this->orderFactory = $orderFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(Observer $observer)
    {
        if (!$this->sysConfig->isOrderProcessingEnabled()) {
            return false;
        }

        /**
         * @var \Magento\Sales\Api\Data\InvoiceInterface $oderCreatePublisher
         */
        $invoiceObj = $observer->getEvent()->getInvoice();
        
        $serviceUrl = $this->sysConfig->getServiceUrl();
        $webhookPath = $this->sysConfig->getOrderSaveAfterWebhook();

        $curl = curl_init();

        if ($this->sysConfig->getServicePort()) {
            $curlOpts[CURLOPT_PORT] = $this->sysConfig->getServicePort();
        }
        
        $curlOpts[CURLOPT_URL] =  $serviceUrl . $webhookPath;
        $curlOpts[CURLOPT_RETURNTRANSFER] =  true;
        $curlOpts[CURLOPT_ENCODING] =  "";
        $curlOpts[CURLOPT_MAXREDIRS] =  10;
        $curlOpts[CURLOPT_TIMEOUT] =  30;
        $curlOpts[CURLOPT_HTTP_VERSION] = CURL_HTTP_VERSION_1_1;
        $curlOpts[CURLOPT_CUSTOMREQUEST] =  "POST";
        $curlOpts[CURLOPT_POSTFIELDS] =  "order_id=" . $invoiceObj->getOrderId();
        $curlOpts[CURLOPT_HTTPHEADER] =  array(
            "Cache-Control: no-cache",
            "Content-Type: application/x-www-form-urlencoded",
            "X-Auth-Key: " . $this->sysConfig->getXAutKey(),
            "X-Auth-User: " . $this->sysConfig->getXAuthUser(),
            "App-Id: " . $this->sysConfig->getAppId(),
            "Token: " . $this->sysConfig->getBearerToken()
        );

        curl_setopt_array($curl, $curlOpts);

        $response = curl_exec($curl);
        $errMsg = curl_error($curl);
        $errNo = curl_errno($curl);
        curl_close($curl);

        if ($errNo) {
            /**
             * If there is an error we need to requeue the order
             * so that it can be re-sent
             * $this->logger->debug("cURL Error #:" . $errMsg . ":" . $errNo);
             */
            $this->logger->info("cURL Response: " . $response . " Error: " . $errNo . ":" . $errMsg);
        }
    }
}
